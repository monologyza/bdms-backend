"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const uuid_1 = require("uuid");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const app = (0, express_1.default)();
const port = 3000;
const dbFilePath = path_1.default.resolve(__dirname, '../db.json');
app.use(express_1.default.json());
//read db file
const readDB = () => {
    try {
        if (!fs_1.default.existsSync(dbFilePath)) {
            writeDB([]);
        }
        const data = fs_1.default.readFileSync(dbFilePath, 'utf-8');
        console.log("Read data from db.json:", data); // Log the raw data
        const parsedData = JSON.parse(data);
        console.log("Parsed todos:", parsedData.todos); // Log the parsed todos
        return parsedData.todos || [];
    }
    catch (error) {
        console.error("Error reading the database file:", error);
        return [];
    }
};
//write db file
const writeDB = (todos) => {
    try {
        const jsonString = JSON.stringify({ todos }, null, 2);
        fs_1.default.writeFileSync(dbFilePath, jsonString);
        console.log("Written data to db.json:", jsonString); // Log the written data
    }
    catch (error) {
        console.error("Error writing to the database file:", error);
    }
};
//create new to do items
app.post("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { taskName, description } = req.body;
    const newTodo = {
        id: (0, uuid_1.v4)(),
        taskName,
        description,
        status: 'todo'
    };
    const todos = readDB();
    todos.push(newTodo);
    writeDB(todos);
    res.status(201).json(newTodo);
}));
//get all todo item
app.get("/todos", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const todos = readDB();
    res.json(todos);
}));
//update todo item
app.put("/todos/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { taskName, description, status } = req.body;
    const todos = readDB();
    const todoIndex = todos.findIndex(todo => todo.id === id);
    //handle status
    if (status !== 'todo' && status !== 'in-process' && status !== 'done') {
        return res.status(400).json({ error: 'Your status is invalid!' });
    }
    if (todoIndex !== -1) {
        todos[todoIndex] = Object.assign(Object.assign({}, todos[todoIndex]), { taskName,
            description,
            status });
        writeDB(todos);
        res.json(todos[todoIndex]);
    }
    else {
        res.status(404).json({ error: 'Todo not found' });
    }
}));
//delete todo item
app.delete("/todos/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const todos = readDB();
    const updatedTodos = todos.filter(todo => todo.id !== id);
    writeDB(updatedTodos);
    res.sendStatus(204);
}));
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
