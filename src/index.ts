import express, {Request, Response} from 'express';
import {v4 as uuidv4} from 'uuid';
import fs from 'fs';
import path from 'path';

const app = express();
const port = 3000;
const dbFilePath = path.resolve(__dirname, '../db.json');

app.use(express.json());

type Status = 'todo' | 'in-process' | 'done';

interface Todo {
    id: string;
    taskName: string;
    description: string;
    status: Status;
}

//read db file
const readDB = (): Todo[] => {
    try {
        if (!fs.existsSync(dbFilePath)) {
            writeDB([]);
        }
        const data = fs.readFileSync(dbFilePath, 'utf-8');
        console.log("Read data from db.json:", data); // Log the raw data
        const parsedData = JSON.parse(data);
        console.log("Parsed todos:", parsedData.todos); // Log the parsed todos
        return parsedData.todos || [];
    } catch (error) {
        console.error("Error reading the database file:", error);
        return [];
    }
};


//write db file
const writeDB = (todos: Todo[]) => {
    try {
        const jsonString = JSON.stringify({todos}, null, 2);
        fs.writeFileSync(dbFilePath, jsonString);
        console.log("Written data to db.json:", jsonString); // Log the written data
    } catch (error) {
        console.error("Error writing to the database file:", error);
    }
};

//create new to do items
app.post("/todos", async (req, res) => {
    const {taskName, description} = req.body;
    const newTodo: Todo = {
        id: uuidv4(),
        taskName,
        description,
        status: 'todo'
    };
    const todos = readDB();
    todos.push(newTodo);
    writeDB(todos);
    res.status(201).json(newTodo);
})
//get all todo item
app.get("/todos", async (req, res) => {
    const todos = readDB();
    res.json(todos);
})

//update todo item
app.put("/todos/:id", async (req, res) => {
    const {id} = req.params;
    const {taskName, description, status} = req.body;
    const todos = readDB();
    const todoIndex = todos.findIndex(todo => todo.id === id);
    //handle status
    if (status !== 'todo' && status !== 'in-process' && status !== 'done') {
        return res.status(400).json({error: 'Your status is invalid!'});
    }
    if (todoIndex !== -1) {
        todos[todoIndex] = {
            ...todos[todoIndex],
            taskName,
            description,
            status
        };
        writeDB(todos);
        res.json(todos[todoIndex]);
    } else {
        res.status(404).json({error: 'Todo not found'});
    }
})

//delete todo item
app.delete("/todos/:id", async (req, res) => {
    const { id } = req.params;
    const todos = readDB();
    const updatedTodos = todos.filter(todo => todo.id !== id);
    writeDB(updatedTodos);
    res.sendStatus(204);
})

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
